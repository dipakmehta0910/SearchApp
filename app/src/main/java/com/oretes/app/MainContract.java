package com.oretes.app;

import java.util.List;

public class MainContract {

    public interface MainContractview{

        void onSuccess(List<String> filteredList);

        void clearAdapter();
    }

    public interface MainContractPresenter {
        void fetchTCODEList(String string);
        List<String> getSearchitems();

        String getDetails(int i);
    }

}
