package com.oretes.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.oretes.app.common.AFPCommonAlertDialog;
import com.oretes.app.common.AFPCommonDialogHandler;
import com.oretes.app.common.Utills;

public class MainActivity extends AppCompatActivity {
    AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.main_container, new MainFragment()).commit();
        MobileAds.initialize(this, getString(R.string.add_mob_app_id));
        adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        if (!Utills.isNetworkAvailable(this)) {
            showNoNetworkDialog();
        }
    }

    private void showNoNetworkDialog() {
        new AFPCommonAlertDialog.Builder(this, AFPCommonAlertDialog.Builder.Type.CONFIRM,
                getString(R.string.no_network))
                .setCommonDialogHandler(new AFPCommonDialogHandler() {
                    @Override
                    public void onDismissDialog() {
                        //unused callback
                    }

                    @Override
                    public void onPositiveButtonClicked() {
                        onBackPressed();
                    }

                    @Override
                    public void onNegativeButtonClicked() {
                        onBackPressed();
                    }
                }).build().show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adView.resume();
    }

    @Override
    protected void onPause() {
        adView.pause();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("onActivityResult", "onActivityResult ");
    }


    private void doSomeNewTest() {
        TestSampleCLass testSampleCLass = new TestSampleCLass();
        try{
            java.lang.reflect.Method method = testSampleCLass.getClass().getMethod("doSome", String.class);
            method.invoke(testSampleCLass, "enableHIPRI");
            //int resultInt = connectivityManager.startUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "enableHIPRI");
            Log.d("ConnectivityManager", "success ");
        } catch (Exception e) {
            Log.d("ConnectivityManager", "execption "+e.getMessage());
        }
    }

}
