package com.oretes.app;

import android.os.Looper;
import android.util.Log;

import com.oretes.app.model.SearchData;
import com.oretes.app.model.SearchResponse;
import com.oretes.app.network.OretesApiClient;
import com.oretes.app.network.OretesApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter implements MainContract.MainContractPresenter {

    private MainContract.MainContractview mMainContractview;
    private List<String> filteredList = new ArrayList<>();
    private List<SearchData> mSearchData = new ArrayList<>();
    private Call<SearchResponse> call;

    MainPresenter(MainContract.MainContractview mainContractview) {
        mMainContractview = mainContractview;
    }

    @Override
    public List<String> getSearchitems() {
        List<String> dummyList = getDummySearchLsit();
        return dummyList;
    }

    @Override
    public String getDetails(int i) {
        try {
            if (mSearchData != null && !mSearchData.isEmpty() && mSearchData.get(i) != null) {
                return mSearchData.get(i).getDetails();
            }
        } catch (IndexOutOfBoundsException e) {
            //nothing to do. will check later
        }
        return "";
    }

    private List<String> getDummySearchLsit() {
        List<String> searchArrayList = new ArrayList<>();
        searchArrayList.add("ram");
        searchArrayList.add("syam");
        searchArrayList.add("sourav");
        searchArrayList.add("gaurav");
        searchArrayList.add("sunil");
        return searchArrayList;
    }

    @Override
    public void fetchTCODEList(String enteredText) {
        System.out.println(" fetchTCODEList " + enteredText);
        OretesApiInterface oretesApiInterface = OretesApiClient.getClient().create(
                OretesApiInterface.class);
        if (call != null) {
            call.cancel();
        }
        call = oretesApiInterface.filterResult(enteredText);
        call.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                final SearchResponse searchResponse = response.body();
                boolean bool = Looper.myLooper() == Looper.getMainLooper();
                Log.d("cool response ", "bool " + bool);
                clearListInSpinner();
                if (searchResponse != null) {
                    if (searchResponse.getSearchdata() != null
                            && !searchResponse.getSearchdata().isEmpty()) {
                        Log.d("cool response ", "cool response "
                                + searchResponse.getSearchdata().size());
                        mSearchData = searchResponse.getSearchdata();
                        for (SearchData searchData : mSearchData) {
                            filteredList.add(searchData.getTCode());
                        }
                        mMainContractview.onSuccess(filteredList);
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                clearListInSpinner();
            }
        });
    }

    private void clearListInSpinner() {
        filteredList.clear();
        mMainContractview.clearAdapter();
        mSearchData.clear();
    }
}
