package com.oretes.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by adminpc on 12/8/18.
 */

public class SearchResponse {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("searchdata")
    @Expose
    private List<SearchData> searchdata = null;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SearchData> getSearchdata() {
        return searchdata;
    }

    public void setSearchdata(List<SearchData> searchdata) {
        this.searchdata = searchdata;
    }

}
