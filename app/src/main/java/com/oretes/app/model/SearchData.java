package com.oretes.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by adminpc on 12/8/18.
 */

public class SearchData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("fl")
    @Expose
    private String fl;
    @SerializedName("sl")
    @Expose
    private String sl;
    @SerializedName("tl")
    @Expose
    private String tl;
    @SerializedName("t_code")
    @Expose
    private String tCode;
    @SerializedName("details")
    @Expose
    private String details;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFl() {
        return fl;
    }

    public void setFl(String fl) {
        this.fl = fl;
    }

    public String getSl() {
        return sl;
    }

    public void setSl(String sl) {
        this.sl = sl;
    }

    public String getTl() {
        return tl;
    }

    public void setTl(String tl) {
        this.tl = tl;
    }

    public String getTCode() {
        return tCode;
    }

    public void setTCode(String tCode) {
        this.tCode = tCode;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

}
