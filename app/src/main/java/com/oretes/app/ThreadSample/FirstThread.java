package com.oretes.app.ThreadSample;

/**
 * Created by adminpc on 12/8/18.
 */

public class FirstThread extends Thread{

    @Override
    public void run() {
        super.run();
        System.out.println("thread first call");
        try {
            Thread.sleep(3400);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("thread end call");
    }
}
