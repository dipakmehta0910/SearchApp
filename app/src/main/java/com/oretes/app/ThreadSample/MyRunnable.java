package com.oretes.app.ThreadSample;

/**
 * Created by adminpc on 12/8/18.
 */

public class MyRunnable implements Runnable{

    @Override
    public void run() {
        System.out.println("MyRunnable first call");
        try {
            Thread.sleep(3400);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("MyRunnable end call");
    }
}
