package com.oretes.app.ThreadSample;

/**
 * Created by adminpc on 12/8/18.
 */

public class MyThreadSample {

    public static void main(String args[]) {
        MyThreadSample myThreadSample = new MyThreadSample();
        myThreadSample.doSomeRunnableTest();
    }

    private void doSomeTest() {
        FirstThread firstThread = new FirstThread();
        System.out.println("before start of thread");
        firstThread.start(); //or firstThread.run()
        System.out.println("after end of thread");
    }

    private void doSomeRunnableTest() {
        Runnable myRunnable = new MyRunnable();
        Thread thread = new Thread(myRunnable);
        System.out.println("before start of Runnable");
        thread.start(); //or thread.run()
        System.out.println("after end of Runnable");
    }

}
