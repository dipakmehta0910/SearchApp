package com.oretes.app.network;

import com.oretes.app.model.SearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by adminpc on 9/5/17.
 */
public interface OretesApiInterface {

    @Headers("Content-Type: application/json")
    @GET("/search/rest/getdata")
    Call<SearchResponse> filterResult (@Query("q") String key);

}
