package com.oretes.app;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class MainFragment extends Fragment implements MainContract.MainContractview {

    private MainContract.MainContractPresenter mMainPresenter;
    private Spinner mSpinner;
    private EditText mEditText;
    private String mString;
    ArrayAdapter<String> adapter;
    TextView descTextView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mMainPresenter = new MainPresenter(this);
        mSpinner = view.findViewById(R.id.fragment_main_spinner);
        mEditText = view.findViewById(R.id.fragment_signup_et_name);
        descTextView = view.findViewById(R.id.fragment_desc_text_view);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                System.out.println(" oretes " + " beforeTextChanged " + charSequence);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                System.out.println(" oretes " + " onTextChanged " + charSequence);
                mString = charSequence.toString();
                mMainPresenter.fetchTCODEList(mString);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                System.out.println(" oretes " + " afterTextChanged " + editable);

            }
        });
        return view;
    }

    private void setSpinnerData(final List<String> filteredList) {
        List<String> dummyList = mMainPresenter.getSearchitems();
        adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, filteredList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.getBackground().setColorFilter(
                ContextCompat.getColor(getActivity(), R.color.colorwhite),
                PorterDuff.Mode.SRC_ATOP);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (filteredList.size() != 0) {
                    descTextView.setText("Details : " + mMainPresenter.getDetails(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onSuccess(List<String> filteredList) {
        setSpinnerData(filteredList);

    }

    @Override
    public void clearAdapter() {
        if (null != adapter) {
            adapter.notifyDataSetChanged();
            descTextView.setText("Details : ");
        }
    }
}
