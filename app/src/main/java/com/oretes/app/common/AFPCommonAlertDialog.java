package com.oretes.app.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.oretes.app.R;

/**
 * Created by tushar on 18/1/18.
 *
 * Common class to get System default alert dialog
 */

public class AFPCommonAlertDialog {

    private String mTitle;
    private String mMessage;
    private String mPositiveButtonTitle;
    private String mNegativeButtonTitle;
    private Context mContext;
    private boolean defaultFocusToPositiveButton;

    private AFPCommonDialogHandler mCommonDialogHandler;
    private Builder.Type mType;

    private AFPCommonAlertDialog(Builder builder) {
        mTitle = builder.mTitle;
        mMessage = builder.mMessage;
        mPositiveButtonTitle = builder.mPositiveButtonTitle;
        mNegativeButtonTitle = builder.mNegativeButtonTitle;
        mContext = builder.mContext;
        mCommonDialogHandler = builder.mCommonDialogHandler;
        defaultFocusToPositiveButton = builder.defaultFocusToPositiveButton;
        mType = builder.mType;
    }

    public void show() {

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.afp_alert_dialog, null);

        alertDialog.setView(dialogView);
        alertDialog.setCancelable(false);

        TextView titleTextView = (TextView) dialogView.findViewById(R.id.alert_title);
        TextView okTextView = (TextView) dialogView.findViewById(R.id.alert_ok);
        TextView cancelTextView = (TextView) dialogView.findViewById(R.id.alert_cancel);
        TextView msgTextView = (TextView) dialogView.findViewById(R.id.alert_msg);


        titleTextView.setText(mTitle);
        msgTextView.setText(mMessage);
        okTextView.setText(mPositiveButtonTitle);
        cancelTextView.setText(mNegativeButtonTitle);

        final AlertDialog dialog = alertDialog.create();
        dialog.show();
        if (mType == Builder.Type.CONFIRM && !defaultFocusToPositiveButton) {
            cancelTextView.requestFocus();
        } else {
            okTextView.requestFocus();
        }
        if (mType == Builder.Type.ERROR) {
            cancelTextView.setVisibility(View.GONE);
        }
        cancelTextView.setVisibility(View.GONE);

        okTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCommonDialogHandler!=null) {
                    mCommonDialogHandler.onPositiveButtonClicked();
                }
                dialog.dismiss();
            }
        });

        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCommonDialogHandler!=null) {
                    mCommonDialogHandler.onNegativeButtonClicked();
                }
                dialog.dismiss();
            }
        });
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if(mCommonDialogHandler!=null) {
                    mCommonDialogHandler.onDismissDialog();
                }
                dialog.dismiss();
            }
        });

    }

    /**
     * Builder to get dialog
     * Context, Dialog type, Message mandatory fields.
     */
    public static class Builder {

        public enum Type {
            CONFIRM,
            ERROR
        }

        private String mTitle;
        private String mMessage;
        private String mPositiveButtonTitle;
        private String mNegativeButtonTitle;
        private Context mContext;

        private Type mType;
        private AFPCommonDialogHandler mCommonDialogHandler;
        private boolean defaultFocusToPositiveButton;

        /**
         * Get Builder instance for Dialog
         * @param context
         * @param type  Dialog type (CONFIRM/ERROR)
         * @param message  Message for alert
         */
        public Builder( Context context,
                Type type,String message) {
            mMessage = message;
            mContext = context;
            mType = type;

            if(mType == Type.CONFIRM) {
                mTitle = "Network Error";
                mPositiveButtonTitle = "OK";
                mNegativeButtonTitle = "Cancel";
            }else {
                /*mTitle = mContext.getString(R.string.fv_alert_error);
                mPositiveButtonTitle = mContext.getString(R.string.fv_alert_dialog_ok);*/
            }

        }

        /**
         * Get Builder instance for Dialog with res id
         *
         * @param context
         */
        public Builder(Context context, Type type, int resoursId) {
            this(context, type, context.getString(resoursId));
        }

        public AFPCommonAlertDialog build() {
            return new AFPCommonAlertDialog(this);
        }

        /**
         * Set Dialog action listener
         * @param commonDialogHandler
         * @return
         */
        public Builder setCommonDialogHandler(AFPCommonDialogHandler commonDialogHandler) {
            mCommonDialogHandler = commonDialogHandler;
            return this;
        }

        /**
         * Set title of dialog
         * @param title
         * @return
         */
        public Builder setTitle(String title) {
            mTitle = title;
            return this;
        }

        /**
         * Set positive button title
         * @param positiveButtonTitle
         * @return
         */
        public Builder setPositiveButtonTitle(String positiveButtonTitle) {
            mPositiveButtonTitle = positiveButtonTitle;
            return this;
        }

        /**
         * Set negative button title
         * @param negativeButtonTitle
         * @return
         */
        public Builder setNegativeButtonTitle(String negativeButtonTitle) {
            mNegativeButtonTitle = negativeButtonTitle;
            return this;
        }

        /**
         * By default focus of alert dialog will be at negative button
         * use this method to change it to positive
         * @param focus
         * @return
         */
        public Builder setDefaultFocusToPositiveButton(boolean focus) {
            this.defaultFocusToPositiveButton = focus;
            return this;
        }

        /**
         * This method is created to create error dialog
         *
         * @param message Message for dialog
         */
        public static Builder createErrorDialog(Context context, String message) {
            return new Builder(context, Type.ERROR, message);
        }

        /**
         * Create confirmation dialog
         * @param context
         * @param message Message for dialog
         * @return
         */
        public static Builder createConfirmationDialog(Context context, String message) {
            return new Builder(context, Type.CONFIRM, message);
        }
    }
}
