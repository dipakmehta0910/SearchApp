package com.oretes.app.common;

/**
 * Created by tushar on 18/1/18.
 *
 * Listener callbacks for Common dialog
 */

public interface AFPCommonDialogHandler {
    void onDismissDialog();
    void onPositiveButtonClicked();
    void onNegativeButtonClicked();
}
