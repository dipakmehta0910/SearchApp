package com.oretes.app.common;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by adminpc on 19/8/18.
 */

public class Utills {


    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager =
                ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isConnected();
    }

}
